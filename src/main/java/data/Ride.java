package data;

import lombok.Data;

@Data
public class Ride {
    Coord startCoord;
    Coord endCoord;
    int possibleBeginTime;
    int possibleEndTime;
    int actualBeginTime;
    int actualEndTime;
    int length = -1;
    int rideIndex;
    boolean taken = false;

    public int getLength() {
         if(length == -1) {
            length = Math.abs(startCoord.x - endCoord.x) + Math.abs(startCoord.y - endCoord.y);
        }
        return length;
    }

    public void assignRide(int actualBeginTime, int actualEndTime){
        this.actualBeginTime = actualBeginTime;
        this.actualEndTime = actualEndTime;
        this.taken = true;
    }

}
