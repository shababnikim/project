package data;

import lombok.Data;

import java.util.List;

@Data
public class SimulationParams {
    private int rows;
    private int columns;
    private int vehicles;
    private int rides_amount;
    private int bonus_rides;
    private int steps;
    private List<Ride> rides_details;
}