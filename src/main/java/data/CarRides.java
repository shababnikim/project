package data;

import lombok.Data;

import java.util.List;

@Data
public class CarRides {
    private int car_index;
    private int car_rides_amount;
    private List<Integer> rides_indexes;
}
