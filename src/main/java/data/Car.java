package data;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Car {
    Coord currCoord;
    int index;
    boolean full = false;
    List<Ride> rides = new ArrayList<>();

    public void chooseRides(List<Ride> rides, int bonus) {
        int t = 0;
        Coord position = new Coord();
        position.setX(0);
        position.setY(0);

        while (true) {

            int bestRideActualStartTime = -1;
            int bestRideActualEndTime = -1;
            int bestRideLength = -1;
            int bestRideBonus = -1;
            Ride bestRide = null;


            for (Ride r : rides) {
                if (r.isTaken()) {
                    continue;
                }
                int rideMinActualStartTime = getMinStartTime(t, position, r);
                if (rideMinActualStartTime == -1) {
                    continue;
                }

                int currBonus = 0;
                if ( rideMinActualStartTime == r.possibleBeginTime) {
                    currBonus = bonus;
                }

                int rideLength = r.getLength();

                // if r is better than current best ride, make it best ride
                if (
                        (bestRideActualStartTime == -1) ||
                        ((rideMinActualStartTime - 10 * currBonus) < (bestRideActualStartTime - 10 * bestRideBonus)) ||
                        ((rideMinActualStartTime - 10 * currBonus == bestRideActualStartTime - 10 * bestRideBonus) && (rideLength < bestRideLength))
                    ) {

                    bestRide = r;
                    bestRideActualStartTime = rideMinActualStartTime;
                    bestRideActualEndTime = rideMinActualStartTime + rideLength;
                    bestRideLength = rideLength;
                    bestRideBonus = currBonus;
                }

            }

            // if we didn't find a possible ride, were done!
            if (bestRide == null) {
                break;
            }

            this.rides.add(bestRide);
            t = bestRideActualEndTime;
            position = bestRide.endCoord;
            bestRide.assignRide(bestRideActualStartTime, bestRideActualEndTime);

        }
    }

    public void chooseRide(List<Ride> rides, int bonus) {
        int t = 0;
        Coord position = new Coord();
        position.setX(0);
        position.setY(0);

            int bestRideActualStartTime = -1;
            int bestRideActualEndTime = -1;
            int bestRideLength = -1;
            int bestRideBonus = -1;
            Ride bestRide = null;


            for (Ride r : rides) {
                if (r.isTaken()) {
                    continue;
                }
                int rideMinActualStartTime = getMinStartTime(t, position, r);
                if (rideMinActualStartTime == -1) {
                    continue;
                }

                int currBonus = 0;
                if ( rideMinActualStartTime == r.possibleBeginTime) {
                    currBonus = bonus;
                }

                int rideLength = r.getLength();

                // if r is better than current best ride, make it best ride
                if (
                        (bestRideActualStartTime == -1) ||
                                ((rideMinActualStartTime - currBonus) < (bestRideActualStartTime - bestRideBonus)) ||
                                ((rideMinActualStartTime - currBonus == bestRideActualStartTime - bestRideBonus) && (rideLength > bestRideLength))
                        ) {

                    bestRide = r;
                    bestRideActualStartTime = rideMinActualStartTime;
                    bestRideActualEndTime = rideMinActualStartTime + rideLength;
                    bestRideLength = rideLength;
                    bestRideBonus = currBonus;
                }

            }

            if (bestRide == null) {
                full= true;
            }else{
                this.rides.add(bestRide);
                t = bestRideActualEndTime;
                position = bestRide.endCoord;
                bestRide.assignRide(bestRideActualStartTime, bestRideActualEndTime);
            }
    }
    // returns -1 if impossible for car to do ride
    private int getMinStartTime(int t, Coord position,Ride r){
        int dist = (position.x-r.startCoord.x)+(position.y-r.startCoord.y);
        if(t> r.possibleEndTime){
            return -1;
        }
        int time = r.possibleBeginTime - t;
        if(dist>time){
            return t+ dist;
        }else{
            return r.possibleBeginTime;
        }
    }
}
