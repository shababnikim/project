package data;

import lombok.Data;

@Data
public class Coord {
    int x;
    int y;
}
