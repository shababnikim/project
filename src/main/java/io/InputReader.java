package io;

import data.Coord;
import data.Ride;
import data.SimulationParams;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ramiloif on 01/03/2018.
 */
public class InputReader {

    public SimulationParams parseFile(String path) throws IOException {
        SimulationParams params = new SimulationParams();
        FileReader fileReader = new FileReader(new File(path));

        BufferedReader br = new BufferedReader(fileReader);
        List<Ride> rides = new ArrayList<>();
        int row = 0;
        int rideIndex = 0;
        String fileLine;
        while ((fileLine = br.readLine()) != null) {
            String [] lineArr = fileLine.split(" ");
            if(row == 0) {
                params.setRows(Integer.parseInt(lineArr[0]));
                params.setColumns(Integer.parseInt(lineArr[1]));
                params.setVehicles(Integer.parseInt(lineArr[2]));
                params.setRides_amount(Integer.parseInt(lineArr[3]));
                params.setBonus_rides(Integer.parseInt(lineArr[4]));
                params.setSteps(Integer.parseInt(lineArr[5]));

            } else {
                Ride ride = new Ride();
                ride.setStartCoord(fromString(lineArr[0] + " " + lineArr[1]));
                ride.setEndCoord(fromString(lineArr[2] + " " + lineArr[3]));
                ride.setPossibleBeginTime(Integer.parseInt(lineArr[4]));
                ride.setPossibleEndTime(Integer.parseInt(lineArr[5]));
                ride.setRideIndex(rideIndex);
                rideIndex++;
               rides.add(ride);
            }
            row++;
        }

        params.setRides_details(rides);
        return params;
    }

    private Coord fromString(String coord) {
        Coord c = new Coord();
        c.setX(Integer.parseInt(coord.split(" ")[0]));
        c.setY(Integer.parseInt(coord.split(" ")[1]));
        return c;
    }
}
