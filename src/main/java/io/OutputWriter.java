package io;

import data.Car;
import data.CarRides;

import java.io.*;
import java.util.List;

public class OutputWriter {
    public void writeResult(List<Car> carsData, String outputFilePath) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File(outputFilePath)));

            carsData.forEach(car -> {
                StringBuffer carDataLine = new StringBuffer();

                carDataLine.append(car.getRides().size() + " ");
                car.getRides().forEach(ride -> {
                    carDataLine.append(ride.getRideIndex() + " ");
                });

                try {
                    writer.write(carDataLine.toString());
                    writer.newLine();
                } catch (IOException e) {
                }
            });

            writer.close();
        } catch (IOException e) {
        }
    }
}
