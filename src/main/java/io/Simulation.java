package io;

import data.Car;
import data.CarRides;
import data.Ride;
import data.SimulationParams;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Simulation {
    @Getter
    private SimulationParams simulationParams;

    public Simulation(SimulationParams simulationParams) {
        this.simulationParams = simulationParams;
    }

    public List<Car> execute_rides() {
        List<Car> cars = new ArrayList<>();

        for (int carIndex = 0; carIndex < this.simulationParams.getVehicles(); carIndex++) {
            Car car = new Car();
            car.setIndex(carIndex);
            car.chooseRides(this.simulationParams.getRides_details(), this.simulationParams.getBonus_rides());
            cars.add(car);
        }

        return cars;
    }

    public List<Car> execute_rides2() {
        List<Car> cars = new ArrayList<>();

        for (int carIndex = 0; carIndex < this.simulationParams.getVehicles(); carIndex++) {
            Car car = new Car();
            car.setIndex(carIndex);
            cars.add(car);
        }

        while (canAsignNewRide()&& hasCars(cars))
            for (int carIndex = 0; carIndex < this.simulationParams.getVehicles(); carIndex++) {
                Car car = cars.get(carIndex);
                car.chooseRide(this.simulationParams.getRides_details(), this.simulationParams.getBonus_rides());

            }
        return cars;
    }

    private boolean canAsignNewRide(){
        for(Ride r :this.simulationParams.getRides_details()) {
            if(!r.isTaken())
                return true;
        }
        return false;
    }

    private boolean hasCars(List<Car> cars) {
        for (Car c : cars) {
            if (!c.isFull()) {
                return true;
            }

        }
        return false;
    }
}
