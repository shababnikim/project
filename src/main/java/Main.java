import data.Car;
import data.CarRides;
import data.Ride;
import data.SimulationParams;
import io.InputReader;
import io.OutputWriter;
import io.Simulation;

import java.io.IOException;
import java.util.List;

public class Main {
    static final String AINPUT_PATH = "in/a_example.in";
    static final String AOUTPUT_PATH = "out/a_example.out";
    static final String BINPUT_PATH = "in/b_should_be_easy.in";
    static final String BOUTPUT_PATH = "out/b_should_be_easy.out";
    static final String CINPUT_PATH = "in/c_no_hurry.in";
    static final String COUTPUT_PATH = "out/c_no_hurry.out";
    static final String DINPUT_PATH = "in/d_metropolis.in";
    static final String DOUTPUT_PATH = "out/d_metropolis.out";
    static final String EINPUT_PATH = "in/e_high_bonus.in";
    static final String EOUTPUT_PATH = "out/e_high_bonus.out";

    public static void main(String [] args){
        System.out.println("hello world");

        fuckar(AINPUT_PATH, AOUTPUT_PATH);
        fuckar(BINPUT_PATH, BOUTPUT_PATH);
        fuckar(CINPUT_PATH, COUTPUT_PATH);
        fuckar(DINPUT_PATH, DOUTPUT_PATH);
        fuckar(EINPUT_PATH, EOUTPUT_PATH);
    }

    private static void fuckar(String in, String out) {
        InputReader inuptReader = new InputReader();
        OutputWriter outputWriter = new OutputWriter();
        SimulationParams simulationParams = null;
        try {
            simulationParams = inuptReader.parseFile(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Simulation simulation = new Simulation(simulationParams);
        List<Car> cars = simulation.execute_rides();
        outputWriter.writeResult(cars, out);
    }
}
